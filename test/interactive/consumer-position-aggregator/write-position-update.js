const Bluebird = require('bluebird')
const { v4: uuid } = require('uuid')

const { config } = require('../../../lib')

function randomStream (category) {
  return `${category}-${uuid()}`
}

const positionUpdates = [
  {
    streamName: randomStream('account:position'),
    event: { id: uuid(), type: 'Recorded', data: { position: 4 } }
  },
  {
    streamName: randomStream('account:command+position'),
    event: { id: uuid(), type: 'Recorded', data: { position: 7 } }
  },
  {
    streamName: randomStream('account:position'),
    event: { id: uuid(), type: 'Recorded', data: { position: 9 } }
  },
  {
    streamName: randomStream('other:position'),
    event: { id: uuid(), type: 'Recorded', data: { position: 15 } }
  },
  {
    streamName: randomStream('other:command+position'),
    event: { id: uuid(), type: 'Recorded', data: { position: 16 } }
  },
  {
    streamName: randomStream('notAPosition'),
    event: { id: uuid(), type: 'Recorded', data: { some: 'data' } }
  }
]

Bluebird.each(positionUpdates, pu =>
  config.messageStore.write(pu.streamName, pu.event)
).then(() => config.consumerPositionAggregator.start())
