const Bluebird = require('bluebird')
const { v4: uuid } = require('uuid')

const { config } = require('../../../lib')

function randomCategory () {
  return uuid().replace(/-/g, '')
}

const positionUpdates = [
  {
    streamName: `${randomCategory()}-${uuid()}`,
    event: { id: uuid(), type: 'Worked', data: { cool: 'stuff' } }
  },
  {
    streamName: `${randomCategory()}-${uuid()}`,
    event: { id: uuid(), type: 'Completed', data: { rad: 'stuff' } }
  },
  {
    streamName: `${randomCategory()}-${uuid()}`,
    event: { id: uuid(), type: 'Finished', data: { bodacious: 'stuff' } }
  },
  {
    streamName: `${randomCategory()}-${uuid()}`,
    event: { id: uuid(), type: 'Terminated', data: { dudical: 'stuff' } }
  },
  {
    streamName: `${randomCategory()}-${uuid()}`,
    event: { id: uuid(), type: 'Expired', data: { gnarly: 'stuff' } }
  },
  {
    streamName: `${randomCategory()}-${uuid()}`,
    event: { id: uuid(), type: 'Settled', data: { hip: 'stuff' } }
  }
]

Bluebird.each(positionUpdates, pu =>
  config.messageStore.write(pu.streamName, pu.event)
).then(() => config.categoryAggregator.start())
