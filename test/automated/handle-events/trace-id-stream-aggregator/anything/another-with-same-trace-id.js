const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const { config } = require('../../../../automated-init')

function randomCategory () {
  return uuid().replace(/-/g, '')
}

test('Observing a traceId for the first time', t => {
  const category = randomCategory()
  const streamName = `${category}-${uuid()}`
  const traceId = uuid()

  const firstMessage = {
    type: 'Firsted',
    metadata: {
      properties: {
        traceId
      }
    },
    data: {},
    streamName,
    position: 1,
    globalPosition: 3,
    time: '2000-01-01T00:00:00.000Z'
  }

  const secondMessage = {
    type: 'Seconded',
    metadata: {
      properties: {
        traceId
      }
    },
    data: {},
    streamName,
    position: 2,
    globalPosition: 5,
    time: '2000-01-01T00:00:01.000Z'
  }

  const { $any } = config.traceIdStreamAggregator.handlers

  return config.db
    .then(() => $any(firstMessage))
    .then(() => $any(secondMessage))
    .then(() =>
      config.db.then(client =>
        client('trace_id_stream')
          .where({ trace_id: traceId })
          .then(([traceIdStream]) => {
            t.assert(traceIdStream, 'Got the stream')

            t.equal(
              traceIdStream.messages.length,
              2,
              'Both messages are in the stream'
            )

            const [firstRetrieved, secondRetrieved] = traceIdStream.messages

            t.equal(
              firstRetrieved.position,
              firstMessage.position,
              'Same position on first'
            )
            t.equal(
              secondRetrieved.position,
              secondMessage.position,
              'Same position on second'
            )
          })
      )
    )
})
