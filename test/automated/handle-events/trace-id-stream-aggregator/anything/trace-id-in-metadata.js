const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const { config } = require('../../../../automated-init')

function randomCategory () {
  return uuid().replace(/-/g, '')
}

test('traceId is directly on metadata', t => {
  const category = randomCategory()
  const streamName = `${category}-${uuid()}`
  const traceId = uuid()

  const message = {
    type: 'NoMatter',
    metadata: {
      traceId
    },
    data: {},
    streamName,
    position: 1,
    globalPosition: 3,
    time: '2000-01-01T00:00:00.000Z'
  }

  const { $any } = config.traceIdStreamAggregator.handlers

  return config.db
    .then(() => $any(message))
    .then(() => $any(message))
    .then(() =>
      config.db.then(client =>
        client('trace_id_stream')
          .where({ trace_id: traceId })
          .then(([traceIdStream]) => {
            t.assert(traceIdStream, 'Got the stream')

            t.equal(
              traceIdStream.messages.length,
              1,
              'Only one message in the stream'
            )

            const [retrieved] = traceIdStream.messages

            // Retrieved message properties
            t.equal(retrieved.streamName, streamName, 'Same streamName')
            t.equal(retrieved.position, message.position, 'Same position')
            t.equal(retrieved.type, message.type, 'Same type')
            t.equal(retrieved.time, message.time, 'Same time')

            // Row properties
            const sequenceAsInt = parseInt(traceIdStream.sequence, 10)
            const timeAsString = traceIdStream.last_updated.toISOString()

            t.equal(sequenceAsInt, message.globalPosition, 'Correct sequence')
            t.equal(timeAsString, message.time, 'Correct last updated time')
          })
      )
    )
})
