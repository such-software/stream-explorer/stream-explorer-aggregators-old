const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const { config } = require('../../../../automated-init')

function randomCategory () {
  return uuid().replace(/-/g, '')
}

test('metadata.properies beats just metadata', t => {
  const category = randomCategory()
  const streamName = `${category}-${uuid()}`
  const propertiesTraceId = uuid()
  const metadataTraceId = uuid()

  const message = {
    type: 'NoMatter',
    metadata: {
      traceId: metadataTraceId,
      properties: {
        traceId: propertiesTraceId
      }
    },
    data: {},
    streamName,
    position: 1,
    globalPosition: 3,
    time: '2000-01-01T00:00:00.000Z'
  }

  const { $any } = config.traceIdStreamAggregator.handlers

  return config.db
    .then(() => $any(message))
    .then(() => $any(message))
    .then(() =>
      config.db.then(client =>
        client('trace_id_stream')
          .where({ trace_id: propertiesTraceId })
          .then(([traceIdStream]) => {
            t.assert(traceIdStream, 'Got the stream')
          })
      )
    )
})
