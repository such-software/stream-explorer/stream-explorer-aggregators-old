const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const Handlers = require('../../../../../lib/trace-id-stream-aggregator/handlers')
const { config } = require('../../../../automated-init')

test('No traceId in message', t => {
  const streamName = `noWrite-${uuid()}`
  const propertiesTraceId = uuid()
  const metadataTraceId = uuid()

  const message = {
    type: 'NoMatter',
    metadata: {},
    data: {},
    streamName,
    position: 1,
    globalPosition: 3,
    time: '2000-01-01T00:00:00.000Z'
  }

  function applyMessage () {
    t.fail('Should not have been called')
  }

  const { $any } = Handlers({ applyMessage })

  return $any(message)
})
