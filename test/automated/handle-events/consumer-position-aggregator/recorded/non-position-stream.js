const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const { config } = require('../../../../automated-init')

test('Observing a Recorded event not in a position stream', t => {
  const streamName = `notAPositionStream`
  const recorded = {
    type: 'Recorded',
    data: {
      some: 'data'
    }
  }
  recorded.streamName = streamName
  recorded.globalPosition = 2
  recorded.time = '2000-01-01T00:00:00'

  const { Recorded } = config.consumerPositionAggregator.handlers

  return config.db
    .then(() => Recorded(recorded))
    .then(() =>
      config.db.then(client =>
        client('consumer_position')
          .where({ stream_name: streamName })
          .then(([consumerPosition]) => {
            t.assert(!consumerPosition, 'Did not get position')
          })
      )
    )
})
