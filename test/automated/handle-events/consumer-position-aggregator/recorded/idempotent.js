const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const { config } = require('../../../../automated-init')

test('Re-observing a Recorded event', t => {
  const positionStreamName = `account:command+position-${uuid()}`
  const position = 10
  const recorded = {
    type: 'Recorded',
    data: {
      position
    }
  }
  recorded.streamName = positionStreamName
  recorded.globalPosition = 2
  recorded.time = '2000-01-01T00:00:00'

  const { Recorded } = config.consumerPositionAggregator.handlers

  const previous = {
    stream_name: positionStreamName,
    position: 20,
    last_updated: '2000-01-01T00:00:00.0000',
    // Greater than the event we're aggregating
    sequence: 4
  }

  return config.db
    .then(client => client('consumer_position').insert(previous))
    .then(() => Recorded(recorded))
    .then(() =>
      config.db.then(client =>
        client('consumer_position')
          .where({ stream_name: positionStreamName })
          .then(([consumerPosition]) => {
            t.assert(consumerPosition, 'Got the position')

            const positionAsInt = parseInt(consumerPosition.position, 10)

            t.equal(positionAsInt, previous.position, 'It did not update')
          })
      )
    )
})
