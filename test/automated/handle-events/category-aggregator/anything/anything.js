const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const { config } = require('../../../../automated-init')

function randomCategory () {
  return uuid().replace(/-/g, '')
}

test('Observing any message', t => {
  const category = randomCategory()
  const streamName = `${category}-${uuid()}`

  const message = {
    type: 'NoMatter',
    data: {},
    streamName,
    globalPosition: 3,
    time: '2000-01-01T00:00:00.000Z'
  }

  const { $any } = config.categoryAggregator.handlers

  return config.db
    .then(() => $any(message))
    .then(() => $any(message))
    .then(() =>
      config.db.then(client =>
        client('category')
          .where({ category })
          .then(([recordedCategory]) => {
            t.assert(recordedCategory, 'Got the category')

            const countAsInt = parseInt(recordedCategory.message_count, 10)
            const sequenceAsInt = parseInt(recordedCategory.sequence, 10)
            const timeAsString = recordedCategory.last_updated.toISOString()

            t.equal(countAsInt, 1, 'Correct count')
            t.equal(sequenceAsInt, message.globalPosition, 'Correct sequence')
            t.equal(timeAsString, message.time, 'Correct time')
          })
      )
    )
})
