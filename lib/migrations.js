const consumerPositionMigration = require('./consumer-position-aggregator/migration')
const categoryMigration = require('./category-aggregator/migration')
const streamMigration = require('./stream-aggregator/migration')
const streamCategoryIndexMigration = require('./stream-aggregator/category-index-migration')
const traceIdStreamMigration = require('./trace-id-stream-aggregator/migration')

module.exports = {
  tableName: 'aggregator_migrations',

  getMigrations () {
    return Promise.resolve([
      'consumer-position',
      'category',
      'stream',
      'stream-category-index',
      'trace-id-stream'
    ])
  },

  getMigrationName (migration) {
    return migration
  },

  getMigration (migration) {
    switch (migration) {
      case 'consumer-position':
        return consumerPositionMigration
      case 'category':
        return categoryMigration
      case 'stream':
        return streamMigration
      case 'stream-category-index':
        return streamCategoryIndexMigration
      case 'trace-id-stream':
        return traceIdStreamMigration
      default:
        return null
    }
  }
}
