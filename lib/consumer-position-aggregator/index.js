const UpsertPosition = require('./queries/upsert-position')
const Handlers = require('./handlers')

function build ({ db, messageStore }) {
  const upsertPosition = UpsertPosition({ db })
  const handlers = Handlers({ upsertPosition })

  const subscription = messageStore.createSubscription({
    streamName: '$all',
    handlers,
    subscriberId: 'consumerPositionsAggregator'
  })

  function start () {
    subscription.start()
  }

  return {
    handlers,
    start
  }
}

module.exports = build
