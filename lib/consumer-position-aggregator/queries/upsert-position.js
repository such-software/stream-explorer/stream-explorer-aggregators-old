function build ({ db }) {
  return function upsertPosition (
    positionStreamName,
    position,
    time,
    sequence
  ) {
    const rawQuery = `
      INSERT INTO 
        consumer_position (stream_name, position, last_updated, sequence)
      VALUES
        (:positionStreamName, :position, :time, :sequence)
      ON
        CONFLICT (stream_name)
        DO UPDATE
          SET
            position = :position,
            last_updated = :time,
            sequence = :sequence
          WHERE consumer_position.sequence < :sequence
    `

    return db.then(client =>
      client.raw(rawQuery, { positionStreamName, position, time, sequence })
    )
  }
}

module.exports = build
