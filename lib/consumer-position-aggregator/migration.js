module.exports = {
  up (knex) {
    return knex.schema.createTable('consumer_position', table => {
      table.string('stream_name').primary()
      table.bigInteger('position')
      table.datetime('last_updated')
      table.bigInteger('sequence').defaultTo(0)
    })
  },

  down (knex) {
    return knex.schema.dropTable('consumer_position')
  }
}
