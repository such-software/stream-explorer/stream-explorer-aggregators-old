const getCategory = require('@suchsoftware/proof-of-concept-message-store/lib/category')

const CATEGORY_TYPE_SEPARATOR = ':'
const COMPOUND_TYPE_DELIMITER = '+'

function isPositionStream (streamName) {
  const category = getCategory(streamName)
  const [_, types] = category.split(CATEGORY_TYPE_SEPARATOR)

  if (!types) {
    return false
  }

  const splitTypes = types.split(COMPOUND_TYPE_DELIMITER)

  return splitTypes[splitTypes.length - 1] === 'position'
}

function build ({ upsertPosition }) {
  return {
    async Recorded (recorded) {
      if (!isPositionStream(recorded.streamName)) {
        return true
      }

      return upsertPosition(
        recorded.streamName,
        recorded.data.position,
        recorded.time,
        recorded.globalPosition
      )
    }
  }
}

module.exports = build
