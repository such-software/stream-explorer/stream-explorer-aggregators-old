const indexName = 'stream_category'

module.exports = {
  up (knex) {
    return knex.schema.alterTable('stream', table => {
      table.index(['category'], indexName)
    })
  },

  down (knex) {
    return knex.schema.alterTable('stream', table => {
      table.index(['category'], indexName)
    })
  }
}
