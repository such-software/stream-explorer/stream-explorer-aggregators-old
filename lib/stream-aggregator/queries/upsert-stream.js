function build ({ db }) {
  return (streamName, category, time, sequence) => {
    const rawQuery = `
      INSERT INTO 
        stream (stream_name, category, message_count, last_updated, sequence)
      VALUES
        (:streamName, :category, 1, :time, :sequence)
      ON
        CONFLICT (stream_name)
        DO UPDATE
          SET
            message_count = stream.message_count + 1,
            last_updated = :time,
            sequence = :sequence
          WHERE stream.sequence < :sequence
    `

    return db.then(client =>
      client.raw(rawQuery, { streamName, category, time, sequence })
    )
  }
}

module.exports = build
