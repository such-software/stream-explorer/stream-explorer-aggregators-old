const getCategory = require('@suchsoftware/proof-of-concept-message-store/lib/category')

function build ({ upsertStream }) {
  return {
    async $any (any) {
      const category = getCategory(any.streamName)

      return upsertStream(
        any.streamName,
        category,
        any.time,
        any.globalPosition
      )
    }
  }
}

module.exports = build
