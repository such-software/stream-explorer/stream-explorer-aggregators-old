const UpsertStream = require('./queries/upsert-stream')
const Handlers = require('./handlers')

function build ({ db, messageStore }) {
  const upsertStream = UpsertStream({ db })
  const handlers = Handlers({ upsertStream })

  const subscription = messageStore.createSubscription({
    streamName: '$all',
    handlers,
    subscriberId: 'streamAggregator'
  })

  function start () {
    subscription.start()
  }

  return {
    handlers,
    start
  }
}

module.exports = build
