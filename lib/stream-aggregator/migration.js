module.exports = {
  up (knex) {
    return knex.schema.createTable('stream', table => {
      table.string('stream_name').primary()
      table.string('category')
      table.bigInteger('message_count')
      table.datetime('last_updated')
      table.bigInteger('sequence').defaultTo(0)
    })
  },

  down (knex) {
    return knex.schema.dropTable('stream')
  }
}
