function build ({ db }) {
  return (category, time, sequence) => {
    const rawQuery = `
      INSERT INTO 
        category (category, message_count, last_updated, sequence)
      VALUES
        (:category, 1, :time, :sequence)
      ON
        CONFLICT (category)
        DO UPDATE
          SET
            message_count = category.message_count + 1,
            last_updated = :time,
            sequence = :sequence
          WHERE category.sequence < :sequence
    `

    return db.then(client => client.raw(rawQuery, { category, time, sequence }))
  }
}

module.exports = build
