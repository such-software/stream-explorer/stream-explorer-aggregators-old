const UpsertCategory = require('./queries/upsert-category')
const Handlers = require('./handlers')

function build ({ db, messageStore }) {
  const upsertCategory = UpsertCategory({ db })
  const handlers = Handlers({ upsertCategory })

  const subscription = messageStore.createSubscription({
    streamName: '$all',
    handlers,
    subscriberId: 'categoryAggregator'
  })

  function start () {
    subscription.start()
  }

  return {
    handlers,
    start
  }
}

module.exports = build
