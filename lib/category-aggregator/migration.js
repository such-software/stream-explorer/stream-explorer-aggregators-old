module.exports = {
  up (knex) {
    return knex.schema.createTable('category', table => {
      table.string('category').primary()
      table.bigInteger('message_count')
      table.datetime('last_updated')
      table.bigInteger('sequence').defaultTo(0)
    })
  },

  down (knex) {
    return knex.schema.dropTable('category')
  }
}
