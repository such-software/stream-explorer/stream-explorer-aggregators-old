const getCategory = require('@suchsoftware/proof-of-concept-message-store/lib/category')

function build ({ upsertCategory }) {
  return {
    async $any (any) {
      const category = getCategory(any.streamName)

      return upsertCategory(category, any.time, any.globalPosition)
    }
  }
}

module.exports = build
