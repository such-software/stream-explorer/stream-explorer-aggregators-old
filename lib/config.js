/*
The system makes heavy use of dependency injection.  This file wires up all the
dependencies, making use of the environment.
*/

const MessageStore = require('@suchsoftware/proof-of-concept-message-store')

const KnexClient = require('./knex-client')
const PostgresClient = require('./postgres-client')

const clock = require('./clock')
const ConsumerPositionAggregator = require('./consumer-position-aggregator')
const CategoryAggregator = require('./category-aggregator')
const StreamAggregator = require('./stream-aggregator')
const TraceIdStreamAggregator = require('./trace-id-stream-aggregator')

function createConfig ({ env }) {
  const knexClient = KnexClient({
    connectionString: env.databaseUrl
  })
  const postgresClient = PostgresClient({
    connectionString: env.messageStoreConnectionString
  })
  const messageStore = MessageStore({ session: postgresClient })
  const consumerPositionAggregator = ConsumerPositionAggregator({
    db: knexClient,
    messageStore
  })
  const categoryAggregator = CategoryAggregator({
    db: knexClient,
    messageStore
  })
  const streamAggregator = StreamAggregator({
    db: knexClient,
    messageStore
  })
  const traceIdStreamAggregator = TraceIdStreamAggregator({
    db: knexClient,
    messageStore
  })

  const components = [
    consumerPositionAggregator,
    categoryAggregator,
    streamAggregator,
    traceIdStreamAggregator
  ]

  return {
    components,
    env,
    db: knexClient,
    messageStore,
    consumerPositionAggregator,
    categoryAggregator,
    streamAggregator,
    traceIdStreamAggregator
  }
}

module.exports = createConfig
