const tableName = 'trace_id_stream'

module.exports = {
  up (knex) {
    return knex.schema.createTable(tableName, table => {
      table.string('trace_id').primary()
      table.jsonb('messages')
      table.datetime('last_updated')
      table.bigInteger('sequence').defaultTo(0)
    })
  },

  down (knex) {
    return knex.schema.dropTable(tableName)
  }
}
