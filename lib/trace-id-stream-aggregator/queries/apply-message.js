function build ({ db }) {
  return (traceId, message, time, sequence) => {
    const rawQuery = `
      INSERT INTO 
        trace_id_stream (trace_id, messages, last_updated, sequence)
      VALUES
        (:traceId, :stringifiedMessage, :time, :sequence)
      ON
        CONFLICT (trace_id)
        DO UPDATE
          SET
            messages = trace_id_stream.messages || :stringifiedMessage,
            last_updated = :time,
            sequence = :sequence
          WHERE trace_id_stream.sequence < :sequence
    `

    const stringifiedMessage = JSON.stringify([
      {
        streamName: message.streamName,
        position: message.position,
        type: message.type,
        time
      }
    ])

    return db.then(client =>
      client.raw(rawQuery, {
        traceId,
        stringifiedMessage,
        time,
        sequence
      })
    )
  }
}

module.exports = build
