function getTraceId (message) {
  if (!message.metadata) {
    return null
  }

  if (!message.metadata.properties) {
    return message.metadata.traceId
  }

  return message.metadata.properties.traceId
}

function build ({ applyMessage }) {
  return {
    async $any (any) {
      const traceId = getTraceId(any)

      if (!traceId) {
        return
      }

      return applyMessage(traceId, any, any.time, any.globalPosition)
    }
  }
}

module.exports = build
