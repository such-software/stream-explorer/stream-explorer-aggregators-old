const ApplyMessage = require('./queries/apply-message')
const Handlers = require('./handlers')

function build ({ db, messageStore }) {
  const applyMessage = ApplyMessage({ db })
  const handlers = Handlers({ applyMessage })

  const subscription = messageStore.createSubscription({
    streamName: '$all',
    handlers,
    subscriberId: 'traceIdStreamAggregator'
  })

  function start () {
    subscription.start()
  }

  return {
    handlers,
    start
  }
}

module.exports = build
